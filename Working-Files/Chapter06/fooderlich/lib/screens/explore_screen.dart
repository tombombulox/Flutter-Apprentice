import 'package:flutter/material.dart';

import '../components/components.dart'; // Where we put all our custom components
import '../models/models.dart'; // The Barrel File all our models.
import '../api/mock_fooderlich_service.dart';

class ExploreScreen extends StatelessWidget {
  // constructor
  ExploreScreen({Key? key}) : super(key: key);

  // class Properties
  final mockServer = MockFooderlichService();

  //build Method
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: mockServer.getExploreData(),
      builder: (context, AsyncSnapshot<ExploreData> snapshot){
        // 4
        // if we got the data back from our ASYNC
        if (snapshot.connectionState == ConnectionState.done){
          // 5
          return ListView(
            scrollDirection: Axis.vertical,
            children: [
              // 7
              TodayRecipeListView(recipes: snapshot.data?.todayRecipes ?? [],),
              // 8
              const SizedBox(height:16),
              // 9
              //TODO: Replace this with FriendPostListView
              FriendPostListView(friendPost: snapshot.data?.friendPosts ?? []),
            ]
          );
          // If We're still waiting for the data back from our ASYNC
        } else {
          return const CircularProgressIndicator();
        }
      }
    );
  }
}
