import 'package:flutter/material.dart';
import 'package:fooderlich/screens/empty_grocery_screen.dart';

class GroceryScreen extends StatelessWidget {
  //Constructor - constant cause it wont change once redendered.
  const GroceryScreen({Key? key}) : super(key: key);

  //build override
  @override
  Widget build(BuildContext context) {
    // TODO 4: Add a scaffold Widget
    return const EmptyGroceryScreen();
  }
  // TODO: Add buildGroceryScreen
}
