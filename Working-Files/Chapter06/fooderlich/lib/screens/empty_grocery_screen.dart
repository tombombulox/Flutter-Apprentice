import 'package:flutter/material.dart';

class EmptyGroceryScreen extends StatelessWidget {
  // constructor
  const EmptyGroceryScreen({Key? key}) : super(key: key);

  // Widget's overriden build method
  @override
  Widget build(BuildContext context) {
    // 1
    return Padding(
        padding: const EdgeInsets.all(30.0),
        // 2
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
                child: AspectRatio(
              aspectRatio: 1 / 1,
              child: Image.asset('assets/fooderlich_assets/empty_list.png'),
            )),
            Text('No Groceries', style: Theme.of(context).textTheme.headline6),
            const SizedBox(height: 16.0),
            const Text(
              'Shopping for ingredients?\n'
              'Tap the + button to write them down!',
              textAlign: TextAlign.center,
            ),
            MaterialButton(
              textColor: Colors.white,
              child: const Text('Browse Recipies'),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              color: Colors.green,
              onPressed: () {
                // TODO 8: Goto Recipes Tab.
              },
            ),
          ],
        )));
  }
}
