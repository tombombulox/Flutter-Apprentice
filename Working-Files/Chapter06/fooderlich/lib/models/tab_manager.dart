import 'package:flutter/material.dart';

//1 - Create a Change Notifier called TabManager
class TabManager extends ChangeNotifier {
  // 2
  int selectedTab = 0;

  // 3
  void goToTab(index) {
    // 4
    selectedTab = index;
    //5
    notifyListeners();
  }

  //6
  void gotToRecipes() {
    selectedTab = 1;
    // 7
    notifyListeners();
  }
}
