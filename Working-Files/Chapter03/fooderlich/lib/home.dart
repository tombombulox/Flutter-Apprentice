import 'package:flutter/material.dart';
import 'package:fooderlich/card1.dart';
import 'package:fooderlich/card2.dart';
import 'package:fooderlich/card3.dart';
import 'home.dart';

// Class signature, our home view inherits
// from StatefuleWidget
class Home extends StatefulWidget {
  // Constructor
  const Home({Key? key}) : super(key: key);

  // we define the state of this widget
  // by pointing it to a private class of type State-of-home
  @override
  _HomeState createState() => _HomeState();
}

// Class Signature, Library Private class HomeState
// extends the State Object of Type Home, defined above.
class _HomeState extends State<Home> {
  //7
  int _selectedIndex = 0;

  // 8
  static List<Widget> pages = <Widget>[
    //TODO: Replace with Card1
    const Card1(),
    // TODO: Replace with Card2
    const Card2(),
    // TODO: Replace with Card3
    const Card3()
  ];

  // 9
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  // Define the data that will change based on state and the functions
  // that will facilitate that state change here (class properties and functions)

  // We override the state build function from
  // from our state class/widget with or view.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fooderlich',
            // 2
            // Here' we're using of() function provided by the Theme class to inherit or pass thru the theme
            // of the widget that THIS widget is used in. So the theme and it's
            // definitions from the MaterialApp() declaration are available to us.
            style: Theme.of(context).textTheme.headline6),
      ),
      //TODO: Show selected tab
      body: pages[_selectedIndex],
      // 4
      bottomNavigationBar: BottomNavigationBar(
          //5
          selectedItemColor:
              Theme.of(context).textSelectionTheme.selectionColor,
          //TODO: Set Selected Tab Bar
          // 6
          //10
          currentIndex: _selectedIndex,
          // 11
          onTap: _onItemTapped,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.card_giftcard),
              label: 'Card',
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.card_giftcard), label: "Card2"),
            BottomNavigationBarItem(
                icon: Icon(Icons.card_giftcard), label: "Card3"),
          ]),
    );
  }
}
