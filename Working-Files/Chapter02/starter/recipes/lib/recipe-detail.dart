// Import Material Widgets
import 'package:flutter/material.dart';
//Import our recipe class, with samples and wuhatnot
import 'recipe.dart';

class RecipeDetail extends StatefulWidget {
  //constructor
  final Recipe recipe;

  const RecipeDetail({
    Key? key,
    required this.recipe,
  }) : super(key: key);

  @override
  _RecipeDetailState createState() {
    return _RecipeDetailState();
  }
}

// Create a library-private instance of  State-for-RecipieDetail objects
// here.
class _RecipeDetailState extends State<RecipeDetail> {
  // This class now has a private property which we'll write code to exploit.
  int _sliderVal = 1;

  // override State<>'s build in build function with our own.
  @override
  Widget build(BuildContext context) {
    // 1
    return Scaffold(
      appBar: AppBar(title: Text(widget.recipe.label)),
      // 2
      body: SafeArea(
        // 3
        child: Column(
          // the children of this column are contained in a list for widget types.
          children: <Widget>[
            // 4
            SizedBox(
                height: 300,
                width: double.infinity,
                child: Image(image: AssetImage(widget.recipe.imageUrl))),
            const SizedBox(
              height: 4,
            ),
            Text(
              widget.recipe.label,
              style: const TextStyle(fontSize: 18),
            ),
            // 7
            Expanded(
              // 8
              child: ListView.builder(
                  itemCount: widget.recipe.ingredients.length,
                  itemBuilder: (BuildContext context, int index) {
                    final ingredient = widget.recipe.ingredients[index];
                    // 9
                    // TODO: Add Ingredient.quantity
                    return Text('${ingredient.quantity * _sliderVal} '
                        '${ingredient.measure} '
                        '${ingredient.name}');
                  }),
            ),

            // TODO: Add SLider() here
            Slider(
              // 10
              min: 1,
              max: 10,
              divisions: 9,
              // 11
              label: '${_sliderVal * widget.recipe.servings} servings',
              // 12
              value: _sliderVal.toDouble(),
              //13
              onChanged: (newValue) {
                setState(() {
                  _sliderVal = newValue.round();
                });
              },
              //14
              activeColor: Colors.green,
              inactiveColor: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
}
