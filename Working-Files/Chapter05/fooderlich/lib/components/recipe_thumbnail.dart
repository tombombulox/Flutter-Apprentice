import 'package:flutter/material.dart';
import 'package:fooderlich/models/models.dart';


class RecipeThumbnail extends StatelessWidget {
  // 1 - Class Properties.
  final SimpleRecipe recipe;

  // Class Consructor
  const RecipeThumbnail({Key? key, required this.recipe}): super(key: key);

  // Concrete implementation
  @override
  Widget build(BuildContext  context){
    //2
    return Container(
      padding: const EdgeInsets.all(8),
      // 3
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // 4 -- Make this thumbnail fill all available space in it's place
          // in the column / row it's in in the gridview.
          Expanded(
            // 5
            child: ClipRRect(
              child: Image.asset('${recipe.dishImage}', fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(12),
            ),
          ),
          // 6
          const SizedBox(height:10 ),
          // 7
          Text(
            recipe.title,
            maxLines: 1,
            style: Theme.of(context).textTheme.bodyText1
          ),
          Text(
            recipe.duration,
            style:Theme.of(context).textTheme.bodyText1
          )
        ],
      )
    );
  }

}
