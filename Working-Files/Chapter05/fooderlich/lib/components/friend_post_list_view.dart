import 'package:flutter/material.dart';

import 'package:fooderlich/models/models.dart';
import 'components.dart';

class FriendPostListView extends StatelessWidget {
  // Class Properties
  final List<Post> friendPost;

  // Class Constructor
const FriendPostListView({Key? key, required this.friendPost}) : super(key: key);

  // Concrete implementation (Build Method)
  @override
  Widget build(BuildContext context){
    return Padding(
      // 2
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 0
      ),
      // 3
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // 4
          Text('Social Chefs 👩‍🍳',
          style: Theme.of(context).textTheme.headline1),
          // 5
          const SizedBox(height: 16),
          ListView.separated(
            // 2
            primary: false,
            // 3
            physics: const NeverScrollableScrollPhysics(),
            // 4
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: friendPost.length,
            itemBuilder: (context, index){
              // 5
              final post = friendPost[index];
              return FriendPostTile(post: post);
            },
            separatorBuilder: (context, index){
              return const SizedBox(height:16);
            },
          ),// 6
          const SizedBox(height: 16),
        ],
    )
    );
  }
}